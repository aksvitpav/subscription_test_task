###################### DEVELOPMENT ##############################
FROM php:8.2-fpm-alpine3.18 as php-dependencies

ENV COMPOSER_HOME=/tmp/composer
ENV APPLICATION_DIRECTORY=/app

RUN apk add icu-dev
RUN apk add --update --no-cache git libzip-dev libpng-dev libpq-dev linux-headers \
    && apk add --no-cache $PHPIZE_DEPS --virtual php-ext-deps \
    && docker-php-ext-configure zip \
    && docker-php-ext-configure bcmath \
    && docker-php-ext-configure gd \
    && docker-php-ext-configure pdo_pgsql \
    && docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-configure intl IPE_ICU_EN_ONLY=1 \
    && docker-php-ext-install zip gd bcmath pdo_pgsql opcache \
    && docker-php-ext-install intl \
    && pecl install xdebug \
    && pecl install redis \
    && apk del php-ext-deps \
    && mkdir -p -m 0777 $APPLICATION_DIRECTORY \
    && mkdir -p -m 0777 $COMPOSER_HOME /tmp/nginx-public

COPY --from=composer:2.5.8 /usr/bin/composer /usr/bin/composer

USER www-data:www-data
WORKDIR $APPLICATION_DIRECTORY

###################### PRODUCTION ###############################
FROM php-dependencies

COPY --chown=www-data:www-data ./auth.json $COMPOSER_HOME/
COPY --chown=www-data:www-data ./composer.json ./composer.lock $APPLICATION_DIRECTORY/

RUN composer install --no-scripts --no-dev

COPY --chown=www-data:www-data . .

RUN composer dump-autoload

EXPOSE 9000
ARG PROJECT_BUILD_VERSION
ENV PROJECT_BUILD_VERSION=$PROJECT_BUILD_VERSION

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["php-fpm"]
