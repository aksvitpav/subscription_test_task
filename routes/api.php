<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserSubscriptionController;
use App\Models\Post;
use App\Models\UserSubscription;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::prefix('subscriptions')->group(function () {
    Route::get('/', [SubscriptionController::class, 'index']);
});

Route::prefix('posts')->group(function () {
    Route::get('/', [PostController::class, 'index']);
    Route::post('/', [PostController::class, 'store'])->middleware(['auth.basic', 'can:create,'.Post::class]);
    Route::prefix('/{postId}')->group(function () {
        Route::get('/', [PostController::class, 'show']);
        Route::patch('/', [PostController::class, 'update'])->middleware(['auth.basic', 'can:update,'.Post::class]);
        Route::delete('/', [PostController::class, 'destroy'])->middleware(['auth.basic', 'can:delete,'.Post::class]);
    });
});

Route::middleware('auth.basic')->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);

    Route::prefix('user')->group(function () {
        Route::get('/', UserController::class);
        Route::prefix('subscriptions')->group(function () {
            Route::get('/', [UserSubscriptionController::class, 'index'])->middleware('can:viewAny,'.UserSubscription::class);
            Route::post('/', [UserSubscriptionController::class, 'store'])->middleware('can:create,'.UserSubscription::class);
            Route::get('/active', [UserSubscriptionController::class, 'showActive'])->middleware('can:view-active,'.UserSubscription::class);
            Route::prefix('{subscriptionId}')->group(function () {
                Route::get('/', [UserSubscriptionController::class, 'show'])->middleware('can:view,'.UserSubscription::class);
                Route::patch('/pay', [UserSubscriptionController::class, 'pay'])->middleware('can:pay,'.UserSubscription::class);
                Route::delete('/', [UserSubscriptionController::class, 'destroy'])->middleware('can:delete,'.UserSubscription::class);
            });
        });
    });
});
