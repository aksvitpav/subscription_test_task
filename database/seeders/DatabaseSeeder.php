<?php

namespace Database\Seeders;

class DatabaseSeeder extends BaseSeeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(AdminSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SubscriptionSeeder::class);

        self::setSequence(
            [
                'admins',
                'users',
                'subscriptions'
            ]
        );
    }
}
