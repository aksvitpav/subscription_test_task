<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;

class UserSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Throwable
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            foreach ($this->getUsers() as $user) {
                User::updateOrCreate(
                    [
                        'email' => $user['email']
                    ],
                    $user
                );
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     *
     * @return array[]
     */
    public function getUsers(): array
    {
        return [
            [
                'name' => 'User',
                'email' => 'user@test.test',
                'password' => Hash::make('12345678'),
                'created_at' => now(),
            ],
        ];
    }
}
