<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

abstract class BaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    abstract public function run(): void;

    /**
     * @param array $tableNames
     *
     * @return void
     */
    protected static function setSequence(array $tableNames): void
    {
        if (DB::getDriverName() === 'pgsql') {
            foreach ($tableNames as $tableName) {
                DB::statement(
                    "SELECT setval(pg_get_serial_sequence('" . $tableName . "', 'id'), coalesce(max(id), 0) + 1, false) FROM ". $tableName
                );
            }
        }
    }
}
