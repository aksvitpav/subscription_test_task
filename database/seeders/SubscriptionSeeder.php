<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Support\Facades\DB;
use Throwable;

class SubscriptionSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Throwable
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            foreach ($this->getSubscriptions() as $subscription) {
                Subscription::updateOrCreate(
                    [
                        'name' => $subscription['name']
                    ],
                    $subscription
                );
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     *
     * @return array[]
     */
    public function getSubscriptions(): array
    {
        return [
            [
                'name' => 'Starter',
                'price' => 9.95,
                'posts_limit' => 10,
                'is_active' => true,
                'created_at' => now(),
            ],
            [
                'name' => 'Professional',
                'price' => 19.95,
                'posts_limit' => 100,
                'is_active' => true,
                'created_at' => now(),
            ],
            [
                'name' => 'Unlimited',
                'price' => 49.95,
                'posts_limit' => 500,
                'is_active' => true,
                'created_at' => now(),
            ],
        ];
    }
}
