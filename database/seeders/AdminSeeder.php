<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;

class AdminSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Throwable
     */
    public function run(): void
    {
        DB::beginTransaction();
        try {
            foreach ($this->getAdmins() as $admin) {
                Admin::updateOrCreate(
                    [
                        'email' => $admin['email']
                    ],
                    $admin
                );
            }
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     *
     * @return array[]
     */
    public function getAdmins(): array
    {
        return [
            [
                'name' => 'Admin',
                'email' => 'admin@test.test',
                'password' => Hash::make('12345678'),
                'created_at' => now(),
            ],
        ];
    }
}
