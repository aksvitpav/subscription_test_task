<?php

namespace App\Traits;

trait DefaultsTrait
{
    public const PAGE = 1;
    public const PER_PAGE = 10;
}
