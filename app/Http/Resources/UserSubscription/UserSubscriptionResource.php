<?php

namespace App\Http\Resources\UserSubscription;

use App\Http\Resources\Subscription\SubscriptionResource;
use App\Models\UserSubscription;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserSubscriptionResource extends JsonResource
{
    /** @var UserSubscription */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'subscription' => SubscriptionResource::make($this->whenLoaded('subscription')),
            'is_active' => $this->resource->is_active,
            'created_post_count' => $this->resource->post_created_count,
            'published_post_count' => $this->resource->post_published_count,
            'expired_at' => $this->resource->expired_at,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }
}
