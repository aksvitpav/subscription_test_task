<?php

namespace App\Http\Resources\UserSubscription;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserSubscriptionCollection extends ResourceCollection
{
    public function toArray(Request $request): AnonymousResourceCollection
    {
        return UserSubscriptionResource::collection($this->collection);
    }
}
