<?php

namespace App\Http\Requests\UserSubscription;

use App\Actions\UserSubscription\GetUserSubscription;
use Illuminate\Foundation\Http\FormRequest;

class PayUserSubscriptionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'required|integer|exists:subscription_user,id',
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'user_id' => $this->user()->id,
            'id' => $this->route('subscriptionId'),
            'subscription_id' => app()->make(GetUserSubscription::class)->execute($this->route('subscriptionId'))->subscription_id,
        ]);
    }


}
