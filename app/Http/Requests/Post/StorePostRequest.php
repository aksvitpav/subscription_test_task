<?php

namespace App\Http\Requests\Post;

use App\Models\Post;
use App\Rules\CheckUserLimits;
use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => Post::validationRules()['title'],
            'text' => Post::validationRules()['text'],
            'is_active' => Post::validationRules()['is_active'],
            'user_id' => [new CheckUserLimits()]
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'user_id' => $this->user()->id,
        ]);
    }


}
