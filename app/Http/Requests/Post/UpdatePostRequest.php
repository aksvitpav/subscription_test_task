<?php

namespace App\Http\Requests\Post;

class UpdatePostRequest extends StorePostRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return parent::rules();
    }

    protected function prepareForValidation(): void
    {
        parent::prepareForValidation();

        $this->merge([
            'id' => $this->route('postId'),
        ]);
    }


}
