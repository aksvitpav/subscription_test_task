<?php

namespace App\Http\Controllers\Auth;

use App\Actions\User\LoginUser;
use App\Actions\User\RegisterUser;
use App\DTOs\UserDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(RegisterRequest $request, RegisterUser $action): JsonResponse|JsonResource
    {
        $dto = UserDto::fromRequest($request);
        return UserResource::make($action->execute($dto));
    }

    public function login(LoginRequest $request, LoginUser $action): JsonResponse|JsonResource
    {
        $dto = UserDto::fromRequest($request);
        return UserResource::make($action->execute($dto));
    }

    public function logout(Request $request): Response
    {
        /** @var Authenticatable $user */
        if ($user = $request->user()) {
            Auth::logout();
        }

        return response('', 204);
    }
}
