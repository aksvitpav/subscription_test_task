<?php

namespace App\Http\Controllers;

use App\Actions\Post\DeletePost;
use App\Actions\Post\GetPost;
use App\Actions\Post\PaginatePosts;
use App\Actions\Post\StorePost;
use App\Actions\Post\UpdatePost;
use App\DTOs\PostDto;
use App\Http\Requests\Post\StorePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Http\Resources\Post\PostCollection;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class PostController extends Controller
{
    public function index(PaginatePosts $action): PostCollection
    {
        $page = request()->query('page', Post::PAGE);
        $perPage = request()->query('per_page', Post::PER_PAGE);

        return PostCollection::make($action->execute($page, $perPage));
    }

    public function store(StorePostRequest $request, StorePost $action): PostResource
    {
        $dto = PostDto::fromRequest($request);

        return PostResource::make($action->execute($dto));
    }

    public function show(Request $request, GetPost $action): PostResource
    {
        return PostResource::make($action->execute($request->route('postId')));
    }

    public function update(UpdatePostRequest $request, UpdatePost $action): PostResource
    {
        $dto = PostDto::fromRequest($request);

        return PostResource::make($action->execute($dto));
    }

    public function destroy(Request $request, DeletePost $action): SymfonyResponse
    {
        $action->execute($request->route('postId'));

        return response(__('No content'), SymfonyResponse::HTTP_NO_CONTENT);
    }
}
