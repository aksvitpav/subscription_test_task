<?php

namespace App\Http\Controllers;

use App\Actions\Subscription\PaginateSubscriptions;
use App\Http\Resources\Subscription\SubscriptionCollection;
use App\Models\Subscription;

class SubscriptionController extends Controller
{
    public function index(PaginateSubscriptions $action): SubscriptionCollection
    {
        $page = request()->query('page', Subscription::PAGE);
        $perPage = request()->query('per_page', Subscription::PER_PAGE);

        return SubscriptionCollection::make($action->execute($page, $perPage));
    }
}
