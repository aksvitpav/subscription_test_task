<?php

namespace App\Http\Controllers;

use App\Actions\UserSubscription\DeleteUserSubscription;
use App\Actions\UserSubscription\GetActiveUserSubscription;
use App\Actions\UserSubscription\GetUserSubscription;
use App\Actions\UserSubscription\PaginateUserSubscriptions;
use App\Actions\UserSubscription\PayUserSubscription;
use App\Actions\UserSubscription\StoreUserSubscription;
use App\DTOs\UserSubscriptionDto;
use App\Http\Requests\UserSubscription\PayUserSubscriptionRequest;
use App\Http\Requests\UserSubscription\StoreUserSubscriptionRequest;
use App\Http\Resources\UserSubscription\UserSubscriptionCollection;
use App\Http\Resources\UserSubscription\UserSubscriptionResource;
use App\Models\UserSubscription;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class UserSubscriptionController extends Controller
{
    public function index(PaginateUserSubscriptions $action): UserSubscriptionCollection
    {
        $page = request()->query('page', UserSubscription::PAGE);
        $perPage = request()->query('per_page', UserSubscription::PER_PAGE);

        return UserSubscriptionCollection::make($action->execute($page, $perPage));
    }

    public function store(StoreUserSubscriptionRequest $request, StoreUserSubscription $action): UserSubscriptionResource
    {
        $dto = UserSubscriptionDto::fromRequest($request);

        return UserSubscriptionResource::make($action->execute($dto));
    }

    public function pay(PayUserSubscriptionRequest $request, PayUserSubscription $action): UserSubscriptionResource
    {
        $dto = UserSubscriptionDto::fromRequest($request);

        return UserSubscriptionResource::make($action->execute($dto));
    }

    public function show(Request $request, GetUserSubscription $action): UserSubscriptionResource
    {
        return UserSubscriptionResource::make($action->execute($request->route('subscriptionId')));
    }

    public function showActive(Request $request, GetActiveUserSubscription $action): UserSubscriptionResource
    {
        return UserSubscriptionResource::make($action->execute($request->user()->id));
    }

    public function destroy(Request $request, DeleteUserSubscription $action)
    {
        $action->execute($request->route('subscriptionId'));

        return response(__('No content'), SymfonyResponse::HTTP_NO_CONTENT);
    }
}
