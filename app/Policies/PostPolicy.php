<?php

namespace App\Policies;

use App\Actions\Post\GetPost;
use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PostPolicy
{
    use HandlesAuthorization;

    protected ?Post $post = null;

    public function __construct(
        protected GetPost $getPost,
    ) {
        if (request()->route('postId')) {
            $this->post = $this->getPost->execute(request()->route('postId'));
        }
    }

    public function viewAny(): Response
    {
        return Response::allow();
    }

    public function view(User $user): Response
    {
        return Response::allow();
    }

    public function create(User $user): Response
    {
        return Response::allow();
    }

    public function update(User $user): Response
    {
        if ($this->post->user_id !== auth()->id()) {
            return Response::deny('This post not belongs to user');
        }

        return Response::allow();
    }

    public function delete(User $user): Response
    {
        if ($this->post->user_id !== auth()->id()) {
            return Response::deny('This post not belongs to user');
        }

        return Response::allow();
    }

    public function restore(User $user): Response
    {
        return Response::allow();
    }
}
