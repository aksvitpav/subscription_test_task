<?php

namespace App\Policies;

use App\Actions\UserSubscription\GetActiveUserSubscription;
use App\Actions\UserSubscription\GetUserSubscription;
use App\Models\User;
use App\Models\UserSubscription;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class UserSubscriptionPolicy
{
    use HandlesAuthorization;

    protected ?UserSubscription $userSubscription = null;

    public function __construct(
        protected GetUserSubscription $getUserSubscription,
        protected GetActiveUserSubscription $getActiveUserSubscription,
    ) {
        if (request()->route('subscriptionId')) {
            $this->userSubscription = $this->getUserSubscription->execute(request()->route('subscriptionId'));
        }
    }

    public function viewAny(User $user): Response
    {
        return Response::allow();
    }

    public function view(User $user): Response
    {
        if (
            $this->userSubscription
            && $this->userSubscription?->user_id !== $user->id
        ) {
            return Response::deny('Subscription not belongs to user');
        }

        return Response::allow();
    }

    public function viewActive(User $user): Response
    {
        if (
            $this->userSubscription
            && $this->userSubscription?->user_id !== $user->id
        ) {
            return Response::deny('Subscription not belongs to user');
        }

        return Response::allow();
    }

    public function create(User $user): Response
    {
        if ($this->getActiveUserSubscription->execute($user->id)) {
            return Response::deny('You already have active subscription');
        }

        return Response::allow();
    }

    public function pay(User $user): Response
    {
        if ($this->getActiveUserSubscription->execute($user->id)) {
            return Response::deny('You already have active subscription');
        }

        if (
            $this->userSubscription
            && $this->userSubscription->expired_at?->gt(now())
        ) {
            return Response::deny('This subscription is already payed');
        }

        if (
            $this->userSubscription
            && $this->userSubscription->expired_at?->lte(now())
        ) {
            return Response::deny('This subscription is already payed and expired');
        }

        return Response::allow();
    }

    public function delete(User $user): Response
    {
        if ($this->userSubscription->user_id !== auth()->id()) {
            return Response::deny('This subscription not belongs to user');
        }

        return Response::allow();
    }

    public function restore(User $user): Response
    {
        return Response::allow();
    }
}
