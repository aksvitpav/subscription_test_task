<?php

namespace App\Repositories\QueryFilters;

use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class AuthorNameFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $column)
    {
        $query->whereHas('user', function ($query) use ($value) {
            $query
                ->where('users.name', 'ilike', '%' . $value . '%');
        });
    }
}
