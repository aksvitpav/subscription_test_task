<?php

namespace App\Repositories;

use App\Interfaces\Repositories\SubscriptionRepositoryInterface;
use App\Models\Subscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class SubscriptionRepository extends AbstractRepository implements SubscriptionRepositoryInterface
{
    public function getModel(): Model
    {
        return new Subscription();
    }

    public function paginateSubscriptions(
        int $page,
        int $perPage,
    ): LengthAwarePaginator {
        $query = $this->getQuery()
            ->where('is_active', true);

        return $query->paginate($perPage, ['*'], 'page', $page);
    }
}
