<?php

namespace App\Repositories;

use App\Interfaces\Repositories\PostRepositoryInterface;
use App\Models\Post;
use App\Models\UserSubscription;
use App\Repositories\QueryFilters\AuthorNameFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\AllowedFilter;

readonly class PostRepository extends AbstractRepository implements PostRepositoryInterface
{
    public function getModel(): Model
    {
        return new Post();
    }

    public function getAllowedFilters(): array
    {
        return [
            AllowedFilter::exact('id'),
            'title',
            AllowedFilter::custom('author.name', new AuthorNameFilter())
        ];
    }

    public function paginatePosts(
        int $page,
        int $perPage,
    ): LengthAwarePaginator {
        $query = $this->getQuery()
            ->where('is_active', true)
            ->with('user');

        $this->addFilters($query);

        return $query->paginate($perPage, ['*'], 'page', $page);
    }

    public function getActivePostsCount(UserSubscription $userSubscription): int
    {
        $query = $this->getQuery();

        return $query
            ->where('posts.user_id', $userSubscription->user_id)
            ->where('posts.user_subscription_id', $userSubscription->id)
            ->where('posts.is_active', true)
            ->count();
    }
}
