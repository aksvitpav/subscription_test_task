<?php

namespace App\Repositories;

use App\Interfaces\Repositories\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

readonly class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    public function getModel(): Model
    {
        return new User();
    }
}
