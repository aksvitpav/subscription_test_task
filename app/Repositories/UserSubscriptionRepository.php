<?php

namespace App\Repositories;

use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;
use App\Models\UserSubscription;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class UserSubscriptionRepository extends AbstractRepository implements UserSubscriptionRepositoryInterface
{
    public function getModel(): Model
    {
        return new UserSubscription();
    }

    public function paginateUserSubscriptions(
        int $page,
        int $perPage,
        int $userId,
    ): LengthAwarePaginator {
        $query = $this->getQuery()
            ->where('user_id', $userId)
            ->with('subscription')
            ->withCount(
                [
                    'posts as post_created_count',
                    'posts as post_published_count' => function (Builder $query) {
                        $query->where('is_active', true);
                    }
                ]
            );

        return $query->paginate($perPage, ['*'], 'page', $page);
    }

    public function getActiveUserSubscription(int $userId): ?Model
    {
        $query = $this->getQuery();

        return $query
            ->where('user_id', $userId)
            ->where('is_active', true)
            ->latest()
            ->with(['subscription'])
            ->first();
    }

    public function deactivateExpiredUserSubscriptions(): void
    {
        $this->getQuery()
            ->whereDate('expired_at', '<=', now())
            ->where('is_active', true)
            ->update([
                'is_active' => false
            ]);
    }
}
