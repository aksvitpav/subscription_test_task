<?php

namespace App\Rules;

use App\Actions\Post\GetActivePostsCount;
use App\Actions\UserSubscription\GetActiveUserSubscription;
use App\Models\UserSubscription;
use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class CheckUserLimits implements DataAwareRule, ValidationRule
{
    protected $data = [];

    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param \Closure(string): PotentiallyTranslatedString $fail
     *
     * @throws BindingResolutionException
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        /** @var GetActiveUserSubscription $getActiveUserSubscription */
        $getActiveUserSubscription = app()->make(GetActiveUserSubscription::class);

        /** @var UserSubscription $userSubscription */
        $userSubscription = $getActiveUserSubscription->execute($value);

        if (! $userSubscription) {
            $fail('You have n`t an active subscription');
            return;
        }

        /** @var GetActivePostsCount $getActivePostsCount */
        $getActivePostsCount = app()->make(GetActivePostsCount::class);
        $postCount = $getActivePostsCount->execute($userSubscription);

        if (
            $userSubscription->subscription->posts_limit <= $postCount
            && $this->data['is_active'] === true
        ) {
            $fail('Your subscription`s post limit has reached');
        }
    }

    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }
}
