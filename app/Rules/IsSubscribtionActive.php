<?php

namespace App\Rules;

use App\Actions\Subscription\GetSubscription;
use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

class IsSubscribtionActive implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param \Closure(string): PotentiallyTranslatedString $fail
     *
     * @throws BindingResolutionException
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        /** @var GetSubscription $getSubscription */
        $getSubscription = app()->make(GetSubscription::class);

        if (! $getSubscription->execute($value)->is_active) {
            $fail('The subscription is not active');
        }
    }
}
