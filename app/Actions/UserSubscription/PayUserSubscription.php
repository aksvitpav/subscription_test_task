<?php

namespace App\Actions\UserSubscription;

use App\DTOs\UserSubscriptionDto;
use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

readonly class PayUserSubscription
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository,
    ) {
    }

    public function execute(UserSubscriptionDto $dto): Model
    {
        $data = Arr::except($dto->toArray(), ['id']);
        $data['is_active'] = true;
        $data['expired_at'] = now()->addMonth();

        $updatedModel = $this->repository->updateById($dto->getId(), $data);

        return $updatedModel->fresh(['subscription']);
    }
}
