<?php

namespace App\Actions\UserSubscription;

use App\DTOs\UserSubscriptionDto;
use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class StoreUserSubscription
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(UserSubscriptionDto $dto): Model
    {
        return $this->repository->create($dto->toArray());
    }
}
