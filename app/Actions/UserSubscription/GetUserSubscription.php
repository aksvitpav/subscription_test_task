<?php

namespace App\Actions\UserSubscription;

use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class GetUserSubscription
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(int $id): Model
    {
        return $this->repository->getById($id, ['subscription']);
    }
}
