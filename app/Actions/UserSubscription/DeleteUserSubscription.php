<?php

namespace App\Actions\UserSubscription;

use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;

readonly class DeleteUserSubscription
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(int $id): void
    {
        $this->repository->deleteById($id);
    }
}
