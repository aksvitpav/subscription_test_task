<?php

namespace App\Actions\UserSubscription;

use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class GetActiveUserSubscription
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(int $userId): ?Model
    {
        return $this->repository->getActiveUserSubscription($userId);
    }
}
