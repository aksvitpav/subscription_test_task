<?php

namespace App\Actions\UserSubscription;

use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class PaginateUserSubscriptions
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(?int $page, ?int $perPage): LengthAwarePaginator
    {
        return $this->repository->paginateUserSubscriptions($page, $perPage, auth()->id());
    }
}
