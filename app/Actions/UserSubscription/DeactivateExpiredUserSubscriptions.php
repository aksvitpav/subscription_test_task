<?php

namespace App\Actions\UserSubscription;

use App\Interfaces\Repositories\UserSubscriptionRepositoryInterface;

readonly class DeactivateExpiredUserSubscriptions
{
    /**
     * @param UserSubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected UserSubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(): void
    {
        $this->repository->deactivateExpiredUserSubscriptions();
    }
}
