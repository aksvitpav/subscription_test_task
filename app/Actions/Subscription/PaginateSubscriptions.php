<?php

namespace App\Actions\Subscription;

use App\Interfaces\Repositories\SubscriptionRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class PaginateSubscriptions
{
    /**
     * @param SubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected SubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(?int $page, ?int $perPage): LengthAwarePaginator
    {
        return $this->repository->paginateSubscriptions($page, $perPage);
    }
}
