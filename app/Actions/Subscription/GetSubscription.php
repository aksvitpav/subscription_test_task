<?php

namespace App\Actions\Subscription;

use App\Interfaces\Repositories\SubscriptionRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class GetSubscription
{
    /**
     * @param SubscriptionRepositoryInterface $repository
     */
    public function __construct(
        protected SubscriptionRepositoryInterface $repository
    ) {
    }

    public function execute(int $id): Model
    {
        return $this->repository->getById($id);
    }
}
