<?php

namespace App\Actions\Post;

use App\DTOs\PostDto;
use App\Interfaces\Repositories\PostRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

readonly class UpdatePost
{
    /**
     * @param PostRepositoryInterface $repository
     */
    public function __construct(
        protected PostRepositoryInterface $repository
    ) {
    }

    public function execute(PostDto $dto): Model
    {
        $updatedModel = $this->repository->updateById($dto->getId(), Arr::except($dto->toArray(), 'id'));

        return $updatedModel->load(['user']);
    }
}
