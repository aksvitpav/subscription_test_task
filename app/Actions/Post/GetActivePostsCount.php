<?php

namespace App\Actions\Post;

use App\Interfaces\Repositories\PostRepositoryInterface;
use App\Models\UserSubscription;

readonly class GetActivePostsCount
{
    /**
     * @param PostRepositoryInterface $repository
     */
    public function __construct(
        protected PostRepositoryInterface $repository
    ) {
    }

    public function execute(UserSubscription $userSubscription): int
    {
        return $this->repository->getActivePostsCount($userSubscription);
    }
}
