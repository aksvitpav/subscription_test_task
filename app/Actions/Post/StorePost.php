<?php

namespace App\Actions\Post;

use App\DTOs\PostDto;
use App\Interfaces\Repositories\PostRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class StorePost
{
    /**
     * @param PostRepositoryInterface $repository
     */
    public function __construct(
        protected PostRepositoryInterface $repository
    ) {
    }

    public function execute(PostDto $dto): Model
    {
        $createdModel = $this->repository->create($dto->toArray());

        return $createdModel->fresh(['user']);
    }
}
