<?php

namespace App\Actions\Post;

use App\Interfaces\Repositories\PostRepositoryInterface;
use Illuminate\Pagination\LengthAwarePaginator;

readonly class PaginatePosts
{
    /**
     * @param PostRepositoryInterface $repository
     */
    public function __construct(
        protected PostRepositoryInterface $repository
    ) {
    }

    public function execute(?int $page, ?int $perPage): LengthAwarePaginator
    {
        return $this->repository->paginatePosts($page, $perPage);
    }
}
