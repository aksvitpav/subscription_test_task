<?php

namespace App\Actions\Post;

use App\Interfaces\Repositories\PostRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class DeletePost
{
    /**
     * @param PostRepositoryInterface $repository
     */
    public function __construct(
        protected PostRepositoryInterface $repository
    ) {
    }

    public function execute(int $id): void
    {
        $this->repository->deleteById($id);
    }
}
