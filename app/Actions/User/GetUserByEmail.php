<?php

namespace App\Actions\User;

use App\Interfaces\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class GetUserByEmail
{
    /**
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        protected UserRepositoryInterface $repository
    ) {
    }

    public function execute(string $email): Model
    {
        return $this->repository->findBy(['email' => $email]);
    }
}
