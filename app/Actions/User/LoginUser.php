<?php

namespace App\Actions\User;

use App\DTOs\UserDto;
use App\Exceptions\LoginException;
use App\Http\Resources\User\UserResource;
use Illuminate\Support\Facades\Auth;

readonly class LoginUser
{
    public function __construct(
        protected StoreUser $storeUser,
    ) {
    }

    /**
     * @throws LoginException
     */
    public function execute(UserDto $dto): UserResource
    {
        $credentials = [
            'email' => $dto->getEmail(),
            'password' => $dto->getPassword(),
        ];

        if (Auth::attempt($credentials, $dto->getRemember())) {
            $user = Auth::getUser();

            return UserResource::make($user);
        }

        throw new LoginException();
    }
}
