<?php

namespace App\Actions\User;

use App\Interfaces\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class GetUser
{
    /**
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        protected UserRepositoryInterface $repository
    ) {
    }

    public function execute(int $id): Model
    {
        return $this->repository->getById($id);
    }
}
