<?php

namespace App\Actions\User;

use App\DTOs\UserDto;
use App\Http\Resources\User\UserResource;

readonly class RegisterUser
{
    public function __construct(
        protected GetUserByEmail $getUserByEmail,
        protected LoginUser $loginUser,
        protected StoreUser $storeUser,
    ) {
    }

    public function execute(UserDto $dto): UserResource
    {
        try {
            $user = $this->getUserByEmail->execute($dto->getEmail());
        } catch (\Throwable $exception) {
            $user = null;
        }

        if (! $user) {
            $this->storeUser->execute($dto);
        }

        return $this->loginUser->execute($dto);
    }
}
