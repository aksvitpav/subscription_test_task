<?php

namespace App\Actions\User;

use App\DTOs\UserDto;
use App\Interfaces\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

readonly class UpdateUser
{
    /**
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        protected UserRepositoryInterface $repository
    ) {
    }

    public function execute(UserDto $dto): Model
    {
        return $this->repository->updateById(
            $dto->getId(),
            Arr::except($dto->toArray(), 'id')
        );
    }
}
