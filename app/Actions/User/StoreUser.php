<?php

namespace App\Actions\User;

use App\DTOs\UserDto;
use App\Interfaces\Repositories\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

readonly class StoreUser
{
    /**
     * @param UserRepositoryInterface $repository
     */
    public function __construct(
        protected UserRepositoryInterface $repository
    ) {
    }

    public function execute(UserDto $dto): Model
    {
        return $this->repository->create($dto->toArray());
    }
}
