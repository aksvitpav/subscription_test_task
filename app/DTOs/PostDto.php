<?php

namespace App\DTOs;

use App\Actions\UserSubscription\GetActiveUserSubscription;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;

final readonly class PostDto
{
    public function __construct(
        protected string $title,
        protected int $userId,
        protected int $userSubscriptionId,
        protected ?string $text = null,
        protected bool $isActive = false,
        protected ?int $id = null,
    ) {
    }

    /**
     * @throws BindingResolutionException
     */
    public static function fromRequest(Request $request): self
    {
        /** @var User $user */
        $user = $request->user();

        return new self(
            title: $request->get('title'),
            userId: $user->id,
            userSubscriptionId: self::getActiveUserSubscriptionId($user),
            text: $request->get('text'),
            isActive: $request->get('is_active'),
            id: $request->input('id'),
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getUserSubscriptionId(): int
    {
        return $this->userSubscriptionId;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function toArray(): array
    {
        return [
            'title' => $this->title,
            'text' => $this->text,
            'is_active' => $this->isActive,
            'user_id' => $this->userId,
            'user_subscription_id' => $this->userSubscriptionId,
        ];
    }

    /**
     * @throws BindingResolutionException
     */
    private static function getActiveUserSubscriptionId(User $user): int
    {
        /** @var GetActiveUserSubscription $getActiveUserSubscription */
        $getActiveUserSubscription = app()->make(GetActiveUserSubscription::class);

        return $getActiveUserSubscription->execute($user->id)->id;
    }
}
