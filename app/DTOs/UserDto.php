<?php

namespace App\DTOs;

use Illuminate\Http\Request;

final readonly class UserDto
{
    public function __construct(
        protected string $email,
        protected bool $remember,
        protected ?string $name = null,
        protected ?string $password = null,
        protected ?int $id = null,
    ) {
    }

    public static function fromRequest(Request $request): self
    {
        return new self(
            email: $request->get('email'),
            remember: $request->get('remember', false),
            name: $request->get('name'),
            password: $request->get('password'),
            id: $request->input('id'),
        );
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getRemember(): bool
    {
        return $this->remember;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function toArray(): array
    {
        $attributes = [
            'name' => $this->name,
            'email' => $this->email,
            'remember' => $this->remember,
        ];

        if ($this->password) {
            $attributes['password'] = $this->password;
        }

        return $attributes;
    }
}
