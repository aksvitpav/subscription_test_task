<?php

namespace App\DTOs;

use Carbon\Carbon;
use Illuminate\Http\Request;

final readonly class UserSubscriptionDto
{
    public function __construct(
        protected int $userId,
        protected int $subscriptionId,
        protected bool $isActive = false,
        protected ?Carbon $expiredAt = null,
        protected ?int $id = null,
    ) {
    }

    public static function fromRequest(Request $request): self
    {
        return new self(
            userId: $request->user()->id,
            subscriptionId: $request->input('subscription_id'),
            isActive: $request->input('is_active', false),
            expiredAt: $request->input('expired_at'),
            id: $request->input('id'),
        );
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getSubscriptionId(): int
    {
        return $this->subscriptionId;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getExpiredAt(): ?Carbon
    {
        return $this->expiredAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->userId,
            'subscription_id' => $this->subscriptionId,
            'is_active' => $this->isActive,
            'expired_at' => $this->expiredAt,
        ];
    }
}
