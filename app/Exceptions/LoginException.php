<?php

namespace App\Exceptions;

use App\Interfaces\CustomExceptionInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LoginException extends \Exception implements CustomExceptionInterface
{
    public function render(Request $request): JsonResponse
    {
        return response()->json(
            [
                'error' => __('auth.failed')
            ],
            Response::HTTP_UNAUTHORIZED
        );
    }
}
