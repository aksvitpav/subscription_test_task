<?php

namespace App\Exceptions;

use App\Interfaces\CustomExceptionInterface;

class CustomException extends \Exception implements CustomExceptionInterface
{
}
