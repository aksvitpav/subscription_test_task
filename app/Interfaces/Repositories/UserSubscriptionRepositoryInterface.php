<?php

namespace App\Interfaces\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface UserSubscriptionRepositoryInterface extends RepositoryInterface
{
    public function paginateUserSubscriptions(int $page, int $perPage, int $userId): LengthAwarePaginator;

    public function getActiveUserSubscription(int $userId): ?Model;

    public function deactivateExpiredUserSubscriptions(): void;
}
