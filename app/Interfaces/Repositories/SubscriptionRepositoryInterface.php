<?php

namespace App\Interfaces\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;

interface SubscriptionRepositoryInterface extends RepositoryInterface
{
    public function paginateSubscriptions(int $page, int $perPage): LengthAwarePaginator;
}
