<?php

namespace App\Interfaces\Repositories;

interface FilteringInterface
{
    public function getAllowedFilters(): array;
    public function getAllowedSorts(): array;
}
