<?php

namespace App\Interfaces\Repositories;

use App\Models\UserSubscription;
use Illuminate\Pagination\LengthAwarePaginator;

interface PostRepositoryInterface extends RepositoryInterface
{
    public function paginatePosts(
        int $page,
        int $perPage,
    ): LengthAwarePaginator;

    public function getActivePostsCount(UserSubscription $userSubscription): int;
}
