<?php

namespace App\Interfaces;

/**
 * Interface CustomExceptionInterface
 *
 * @package App\Interfaces\Repositories
 */
interface CustomExceptionInterface extends \Throwable
{
}
