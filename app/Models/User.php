<?php

namespace App\Models;

use Filament\Models\Contracts\FilamentUser;
use Filament\Panel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use WendellAdriel\Lift\Attributes\Cast;
use WendellAdriel\Lift\Attributes\Fillable;
use WendellAdriel\Lift\Attributes\Hidden;
use WendellAdriel\Lift\Attributes\Relations\HasMany;
use WendellAdriel\Lift\Attributes\Rules;
use WendellAdriel\Lift\Lift;

#[HasMany(Subscription::class)]
class User extends Authenticatable implements FilamentUser
{
    use HasApiTokens, HasFactory, Notifiable, Lift;

    #[Cast('int')]
    public int $id;

    #[Fillable]
    #[Rules(['required', 'string'])]
    public string $name;

    #[Fillable]
    #[Rules(['required', 'email'])]
    public string $email;

    #[Fillable]
    #[Hidden]
    #[Rules(['required', 'min:8', 'max:255'])]
    #[Cast('hashed')]
    public string $password;

    #[Hidden]
    #[Rules(['nullable', 'max:100'])]
    public ?string $remember_token;

    #[Rules(['nullable', 'datetime'])]
    #[Cast('datetime')]
    public ?string $email_verified_at;

    public function canAccessPanel(Panel $panel): bool
    {
        return false;
    }
}
