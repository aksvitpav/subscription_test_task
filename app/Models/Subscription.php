<?php

namespace App\Models;

use App\Traits\DefaultsTrait;
use Illuminate\Database\Eloquent\Model;
use WendellAdriel\Lift\Attributes\Cast;
use WendellAdriel\Lift\Attributes\Fillable;
use WendellAdriel\Lift\Attributes\Rules;
use WendellAdriel\Lift\Lift;

class Subscription extends Model
{
    use Lift;
    use DefaultsTrait;

    #[Cast('int')]
    public int $id;

    #[Fillable]
    #[Rules(['required', 'string'])]
    public string $name;

    #[Rules(['required', 'numeric'])]
    #[Fillable]
    #[Cast('float')]
    public float $price;

    #[Rules(['required', 'integer'])]
    #[Fillable]
    #[Cast('int')]
    public int $posts_limit;

    #[Rules(['nullable', 'boolean'])]
    #[Fillable]
    #[Cast('boolean')]
    public bool $is_active;
}
