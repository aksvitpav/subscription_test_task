<?php

namespace App\Models;

use App\Traits\DefaultsTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use WendellAdriel\Lift\Attributes\Cast;
use WendellAdriel\Lift\Attributes\Fillable;
use WendellAdriel\Lift\Attributes\Relations\BelongsTo;
use WendellAdriel\Lift\Attributes\Relations\HasMany;
use WendellAdriel\Lift\Attributes\Rules;
use WendellAdriel\Lift\Lift;

#[BelongsTo(Subscription::class)]
#[HasMany(Post::class)]
class UserSubscription extends Model
{
    use Lift;
    use DefaultsTrait;

    protected $table = 'subscription_user';

    #[Cast('int')]
    public int $id;

    #[Rules(['required', 'integer', 'exists:users,id'])]
    #[Fillable]
    #[Cast('int')]
    public int $user_id;

    #[Rules(['required', 'integer', 'exists:subscriptions,id'])]
    #[Fillable]
    #[Cast('int')]
    public int $subscription_id;

    #[Rules(['nullable', 'boolean'])]
    #[Fillable]
    #[Cast('boolean')]
    public bool $is_active;

    #[Rules(['nullable', 'date'])]
    #[Fillable]
    #[Cast('datetime')]
    public ?Carbon $expired_at;
}
