<?php

namespace App\Models;

use App\Traits\DefaultsTrait;
use Illuminate\Database\Eloquent\Model;
use WendellAdriel\Lift\Attributes\Cast;
use WendellAdriel\Lift\Attributes\Fillable;
use WendellAdriel\Lift\Attributes\Relations\BelongsTo;
use WendellAdriel\Lift\Attributes\Rules;
use WendellAdriel\Lift\Lift;

#[BelongsTo(User::class)]
#[BelongsTo(UserSubscription::class)]
class Post extends Model
{
    use Lift;
    use DefaultsTrait;

    #[Cast('int')]
    public int $id;

    #[Fillable]
    #[Cast('string')]
    #[Rules(['required', 'string', 'max:255'])]
    public string $title;

    #[Fillable]
    #[Rules(['nullable', 'max:10000'])]
    #[Cast('string')]
    public ?string $text;

    #[Rules(['nullable', 'boolean'])]
    #[Fillable]
    #[Cast('boolean')]
    public bool $is_active;

    #[Rules(['required', 'integer', 'exists:users,id'])]
    #[Fillable]
    #[Cast('int')]
    public int $user_id;

    #[Rules(['required', 'integer'])]
    #[Fillable]
    #[Cast('int')]
    public int $user_subscription_id;
}
