<?php

namespace App\Jobs;

use App\Actions\UserSubscription\DeactivateExpiredUserSubscriptions;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckSubscriptionPeriod implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected DeactivateExpiredUserSubscriptions $checkSubscriptionPeriod;

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->checkSubscriptionPeriod = app()->make(DeactivateExpiredUserSubscriptions::class);
        $this->checkSubscriptionPeriod->execute();
    }
}
